using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PickPokemon : MonoBehaviour
{
    [SerializeField]
    MainTrainer m_Entrenador;

    [SerializeField]
    Pokemon[] m_firstPokemons;

    [SerializeField]
    GameObject inputField;

    [SerializeField]
    GameObject m_PickInputCanvas;
    [SerializeField]
    GameObject m_ImagePokemon;


    private void OnTriggerEnter2D(Collider2D collision)
    {
       if(collision.tag!="Untagged")
        {
            Debug.Log(collision.tag);
            switch (collision.tag)
            {
                case "Pokemon1":
                    m_Entrenador.m_Pokemons[0] = new PokemonCapturat("tmp", m_firstPokemons[0]);
                    break;
                case "Pokemon2":
                    m_Entrenador.m_Pokemons[0] = new PokemonCapturat("tmp", m_firstPokemons[1]);
                    break;
                case "Pokemon3":
                    m_Entrenador.m_Pokemons[0] = new PokemonCapturat("tmp", m_firstPokemons[2]);
                    break;
            }
            m_PickInputCanvas.SetActive(true);
            m_ImagePokemon.GetComponent<Image>().sprite = m_Entrenador.m_Pokemons[0].m_Pok.m_SpriteF;
            GetComponent<Movement>().canMove = false;

        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "Finish" && m_Entrenador.m_Pokemons[0] != null)
        {
            SceneManager.LoadScene("Agustin");
        }
    }

    public void SetNamePokemon()
    {
        Debug.Log(inputField.GetComponent<TMP_InputField>().text);
        m_Entrenador.m_Pokemons[0].m_Nom = inputField.GetComponent<TMP_InputField>().text;
        m_PickInputCanvas.SetActive(false);
        GetComponent<Movement>().canMove = true;
    }

   


}
