
using System.Collections;
using UnityEngine;

public class Moving : MonoBehaviour
{

    Rigidbody2D m_RigidBody;
    Animator m_animator;

    [SerializeField]
    private GameEvent FoundPokemonInBush;

    [SerializeField]
    MainTrainer m_MainTrainer;


    private int m_dirN = 0;
    public readonly string[] IdleDirections = { "IdleW", "IdleWD", "IdleD", "IdleSD", "IdleS", "IdleSA", "IdleA", "IdleWA" };
    public readonly string[] RunDirections = { "RunW", "RunWD", "RunD", "RunSD", "RunS", "RunSA", "RunA", "RunWA" };
    //   0         1       2       3       4       5       6       7
    [SerializeField]
    private float CharacterSpeedWalk = 1.0f;
    [SerializeField]
    private float CharacterSpeedRun = 2.0f;
    private float CharacterSpeedActual = 1.0f;

    private Vector2 InputDir;
    private float VertInput;
    private float HorInput;
    private Vector2 WhereAmI;
    private Vector2 WhereTo;

    [SerializeField]
    Pokemon[] m_firstPokemons;

    private bool canMove = true;


    [SerializeField]
    private int bushChance = 10;
    private bool EvitarSpamBush=false;


    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (canMove)
        {
            VertInput = Input.GetAxis("Vertical");
            HorInput = Input.GetAxis("Horizontal");
            if (Input.GetKey(KeyCode.Space))
                CharacterSpeedActual = CharacterSpeedRun;
            else
                CharacterSpeedActual = CharacterSpeedWalk;
            if (InputDir != new Vector2(HorInput, VertInput).normalized)
            {
                InputDir = new Vector2(HorInput, VertInput).normalized;
                CheckDireccio(InputDir);
            }
            WhereAmI = m_RigidBody.position;
            WhereTo = WhereAmI + (InputDir * CharacterSpeedActual) * Time.fixedDeltaTime;
            m_RigidBody.MovePosition(WhereTo);
        }
        if(Input.GetKey(KeyCode.O))
            GameManager.Instance.Saveinfo();
        if (Input.GetKey(KeyCode.P))
            GameManager.Instance.LoadGameInfo();

    }

    void changeCanMove(bool t)
    {
        this.canMove = t;
    }

    void CheckDireccio(Vector2 direccion)
    {
        if (direccion.x < 0)            //va a la izquierda
        {
            if (direccion.y > 0)        //va arriba = WA
            {
                m_dirN = 7;
            }
            else if (direccion.y < 0)   //va abajo = SA
            {
                m_dirN = 5;
            }
            else                        //no va vertical = A
            {
                m_dirN = 6;
            }

        }
        else if (direccion.x > 0)          //va a la derecha
        {
            if (direccion.y > 0)        //va arriba = WD
            {
                m_dirN = 1;
            }
            else if (direccion.y < 0)   //va abajo = SD
            {
                m_dirN = 3;
            }
            else                        //no va vertical = D
            {
                m_dirN = 2;
            }
        }
        else                            //no va a lados
        {
            if (direccion.y > 0)        //va arriba = W
            {
                m_dirN = 0;
            }
            else if (direccion.y < 0)   // va abajo = S
            {
                m_dirN = 4;
            }
            else                        //no va vertical = NADA
            {
                m_animator.Play(IdleDirections[m_dirN]);
                return;
                //tendria q quedarse quieto mirando a donde iba
            }
        }
        m_animator.Play(RunDirections[m_dirN]);
    }

 

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Bush" && InputDir != new Vector2(0, 0) && !EvitarSpamBush)
        {
            EvitarSpamBush = true;
            StartCoroutine(WaitBush());
        }
    }


    


    private void MovingInBush()
    {
        if (Random.Range(0, 100) <= bushChance)
        {
            FoundPokemonInBush.Raise();
        }   
        
    }
    
    IEnumerator WaitBush()
    {
        MovingInBush();
        yield return new WaitForSeconds(0.8f);
        EvitarSpamBush = false;

    }
    public void Load(SavePlayerPosition data) 
    {
        Debug.Log("Carregant posicio informació y data del player");
        transform.position = data.player.position;
    }

    public void LoadPartida(SavePlayerPosition data)
    {
        Debug.Log("Carregant posicio informació y data del player");
        transform.position = data.player.position2;
        Debug.Log("nombre del pok " + data.player.pokemonname.ToString() + " / nom custom: "+data.player.pokemonnamepers.ToString());
        switch (data.player.pokemonname.ToString())
        {
            case "Bulbasaur":
                m_MainTrainer.m_Pokemons[0] = new PokemonCapturat(data.player.pokemonnamepers.ToString(), m_firstPokemons[0]);
                break;
            case "Charmander":
                m_MainTrainer.m_Pokemons[0] = new PokemonCapturat(data.player.pokemonnamepers.ToString(), m_firstPokemons[1]);
                break;
            case "Squirtle":
                m_MainTrainer.m_Pokemons[0] = new PokemonCapturat(data.player.pokemonnamepers.ToString(), m_firstPokemons[2]);
                break;
            default:
                m_MainTrainer.m_Pokemons[0] = new PokemonCapturat(data.player.pokemonnamepers.ToString(), m_firstPokemons[2]);
                break;
        }
       
      

    }


}
