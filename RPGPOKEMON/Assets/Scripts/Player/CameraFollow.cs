
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    [SerializeField]
    private Vector3 offset;
    [SerializeField]
    private float damping;

    private Vector3 movePosition;
    private float m_X,m_Y;


    private Vector3 velocity = Vector3.zero;


    private void FixedUpdate()
    {
        m_X = target.position.x;
        m_Y = target.position.y;

        movePosition = new Vector3(m_X, m_Y, 0) + offset;
    
        transform.position = Vector3.SmoothDamp(transform.position, movePosition, ref velocity, damping);

    }
}
