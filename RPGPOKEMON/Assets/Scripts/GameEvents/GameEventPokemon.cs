using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventPokemon", menuName = "GameEvent/GameEvent - Pokemon")]
public class GameEventPokemon : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GameEventPokemonListener> eventListeners =
        new List<GameEventPokemonListener>();

    public void Raise(Pokemon info)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(info);
    }

    public void RegisterListener(GameEventPokemonListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventPokemonListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}

