using UnityEngine.Events;
using UnityEngine;

public class GameEventPokemonListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventPokemon Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<Pokemon> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(Pokemon info)
    {
        Response.Invoke(info);
    }
}
