using OdinSerializer;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using Scene = UnityEngine.SceneManagement.Scene;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;
    public static GameManager Instance
    {
        get { return m_Instance; }
    }

    //[SerializeField]
    //Pool m_PoolPorSi;
    //public Pool Pool => m_PoolPorSi;


    [SerializeField]
    private Pokemon[] m_WildPokemons;

    [SerializeField]
    GameObject m_Prefab;

    [SerializeField]
    GameObject m_EntrenadorEnemic;

    [SerializeField]
    PC PC;

    [SerializeField]
    Vector2 m_EnemyDirection;

    public string m_SceneGuardada;

    [SerializeField]
    PokemonCombat m_emptyPokemon;

    public PokemonCapturat m_PrimerPokemon;

    [SerializeField]
    MainTrainer m_Entrenador;

    private SavePlayerPosition m_CurrentSavedData;

    public void SavePosition()
    {
        SavePlayerPosition savePlayerPosition =new SavePlayerPosition();
        Moving moving =FindObjectOfType<Moving>();
        savePlayerPosition.player.position=moving.transform.position;

        byte[] serializedData = SerializationUtility.SerializeValue<SavePlayerPosition>(savePlayerPosition, DataFormat.JSON);
        string base64 = System.Convert.ToBase64String(serializedData);

        File.WriteAllText("savegame.json", base64);
        Debug.Log("Desant el fitxer savegame.json");
        Debug.Log(savePlayerPosition);
    }
    public void Saveinfo()
    {
        SavePlayerPosition savePlayerPosition = new SavePlayerPosition();
        Moving moving = FindObjectOfType<Moving>();
        savePlayerPosition.player.position2 = moving.transform.position;
        savePlayerPosition.player.pokemonname = m_Entrenador.m_Pokemons[0].m_Pok.name.ToString();
        savePlayerPosition.player.pokemonnamepers = m_Entrenador.m_Pokemons[0].m_Nom.ToString();


        byte[] serializedData = SerializationUtility.SerializeValue<SavePlayerPosition>(savePlayerPosition, DataFormat.JSON);
        string base64 = System.Convert.ToBase64String(serializedData);

        File.WriteAllText("savegame.json", base64);
        Debug.Log("Desant el fitxer savegame.json");
        Debug.Log(savePlayerPosition);
    }
    public void LoadGame() 
    {
        try
        {
            Debug.Log("Carregant el fitxer: savegame.json");
            string newBase64 = File.ReadAllText("savegame.json");
            byte[] serializedData = System.Convert.FromBase64String(newBase64);

            m_CurrentSavedData = SerializationUtility.DeserializeValue<SavePlayerPosition>(serializedData, DataFormat.JSON);
            Debug.Log(m_CurrentSavedData);
            //subscribe to the scene loaded
            SceneManager.sceneLoaded += LoadGameSceneLoaded;
            //change scene to the specified one
            //Debug.Log("Canviant a escena: " + m_CurrentSavedData.level);
            FindObjectOfType<Moving>().Load(m_CurrentSavedData);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    public void LoadGameInfo()
    {
        try
        {
            Debug.Log("Carregant el fitxer: savegame.json AMB DADES DEL PJ Y INVENTARI");
            string newBase64 = File.ReadAllText("savegame.json");
            byte[] serializedData = System.Convert.FromBase64String(newBase64);

            m_CurrentSavedData = SerializationUtility.DeserializeValue<SavePlayerPosition>(serializedData, DataFormat.JSON);
            Debug.Log(m_CurrentSavedData);
            //subscribe to the scene loaded
            //SceneManager.sceneLoaded += LoadGameSceneLoaded;
            //change scene to the specified one
            //Debug.Log("Canviant a escena: " + m_CurrentSavedData.level);
            FindObjectOfType<Moving>().LoadPartida(m_CurrentSavedData);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    private void LoadGameSceneLoaded(Scene scene,LoadSceneMode mode)
    {
        Debug.Log("Escena carregada en mode LOAD : "+scene.name);
        //we are called because we are loading and the scene has been loaded
        //load values to the player
        FindObjectOfType<Moving>().Load(m_CurrentSavedData);
        //unsubscribe from the onloadscene
        SceneManager.sceneLoaded -= LoadGameSceneLoaded;
    }
    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        if(m_Entrenador.m_Pokemons.Length > 0)
            m_PrimerPokemon = m_Entrenador.m_Pokemons[0];

    }
    public void BushBattle()
    {
        m_SceneGuardada = SceneManager.GetActiveScene().name;
        m_emptyPokemon.LoadInfo(m_WildPokemons[Random.Range(0, m_WildPokemons.Length)]);
        SavePosition();
        SceneManager.LoadScene("Hang");
    }

    public void EntremEnCombat()
    {
        Debug.Log("Soy game gamanager y recibi aviso de pokemon en bush");
        m_SceneGuardada = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene("BattleScene");
        foreach(PokemonCapturat p in m_Entrenador.m_Pokemons)
        {
            if (p.Vivo)
            {
                m_PrimerPokemon = p;
                break;
            }
        }
    }
    public void SurtimDelCombat()
    {
        SceneManager.LoadScene(m_SceneGuardada);
        //Cargar Todo el personaje principal
    }
    public void EntroRuta1()
    {
        SceneManager.LoadScene("Ruta 1");
        //Cargar Todo el personaje principal
    }
    public void SalgoRuta1()
    {
        SceneManager.LoadScene("Pueblo Chuleta");
        //Cargar Todo el personaje principal
    }
    public void RecollirObjecte(ObjecteUsable obj)
    {
        if (m_Entrenador.m_Inventari.m_ObjectesUsables.ContainsKey(obj))
        {
            m_Entrenador.m_Inventari.m_ObjectesUsables[obj] += 1;
        }
        else
        {
            m_Entrenador.m_Inventari.m_ObjectesUsables.Add(obj, 1);
        }
        Debug.Log("TENGO "+ m_Entrenador.m_Inventari.m_ObjectesUsables[obj] +
            " de "+obj.m_Nom+ " que cura "+obj.m_VidaCura);
    }

    public void ComprarObj(int quantitat, ObjecteUsable obj)
    {
        while(quantitat > 0)
        {
            RecollirObjecte(obj);
            quantitat--;
        }
    }
    public void CapturarPokemon(PokemonCombat pok)
    {
        PokemonCapturat nuevo = new PokemonCapturat(pok.m_Nom, pok.m_Pok, pok.m_Pok.m_Nivell, 0, pok.m_Atacs, pok.m_Pok.m_Sprite, true);

        if(m_Entrenador.m_Pokemons.Length >= 6)
        {
            PC.m_Pokemons[PC.m_Pokemons.Length] = nuevo;
        }
        else
        {
            m_Entrenador.m_Pokemons[m_Entrenador.m_Pokemons.Length] = nuevo;
        }
           

    }
    
    public void EntrenadorTrobat()
    {

        RaycastHit2D hit = Physics2D.Raycast(transform.position, m_EnemyDirection, GetComponent<CircleCollider2D>().radius * 1.3f);
        if (hit.collider.gameObject == m_Prefab)
        {
            //Pones el personaje en modo combate con un booleano y haces lo siguiente
            Vector2 Origin = new Vector2(m_EntrenadorEnemic.transform.position.x, m_EntrenadorEnemic.transform.position.y);
            Vector2 Desti = new Vector2(m_Prefab.transform.position.x, m_Prefab.transform.position.y);
            Vector2 Direccio = Desti - Origin;
            m_EntrenadorEnemic.GetComponent<Rigidbody2D>().velocity = Direccio.normalized;
            EntremEnCombat();
        }

    }
    // Hacer funcion para Generar RayCast para hacer la vision de los entrenadores y tal vez reset de coses, tu sabras el que fas Alex del Futur

}
