using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionOnGround : MonoBehaviour
{
    [SerializeField]
    ObjecteUsable jomateix;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameManager.Instance.RecollirObjecte(jomateix);
            Destroy(gameObject);
        }
            

    }
    
}
