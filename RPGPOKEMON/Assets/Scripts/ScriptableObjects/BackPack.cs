using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BackPack", menuName = "Pokemon/BackPack")]
public class BackPack : ScriptableObject
{
    public Dictionary<ObjecteUsable, int> m_ObjectesUsables = new Dictionary<ObjecteUsable, int>();
}
