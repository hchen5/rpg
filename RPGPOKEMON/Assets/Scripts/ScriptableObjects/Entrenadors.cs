using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Entrenador", menuName = "Pokemon/Entrenador")]
public class Entrenadors : ScriptableObject
{
    public string m_Nom;
    public string m_Desc;
    public Sprite m_Sprite;
    public Pokemon[] m_Pokemons = new Pokemon[6];

}
