using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "Attacks", menuName = "Pokemon/Attacks")]
public class Atacs : ScriptableObject
{
    public string m_Nom;
    public int m_PPsMaxims;
    public int m_PPsActuals;
    public int m_DMG;
    public int m_Precisio;
    public Tipus m_Type;
    public TipusAtac m_TypeAtac;
    public Sprite m_SpriteBoto;
    public Sprite m_SpriteAtac;

    public bool Ataquer(PokemonCombat atacado)
    {
        float multiplier = 1;

        switch (m_Type)
        {
            case Tipus.Foc:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Herba:
                    case Tipus.Ferro:
                    case Tipus.Gel:
                    case Tipus.Bicho:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Ferro:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Psiquic:
    
                    case Tipus.Gel:
                    case Tipus.Normal:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Aigua:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Terra:
                    case Tipus.Foc:
                    case Tipus.Normal:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Bicho:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Psiquic:

                    case Tipus.Herba:
                    case Tipus.Sinistre:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Drac:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Drac:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Electric:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Aigua:
                    case Tipus.Volador:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Fantasma:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Fantasma:
                    case Tipus.Volador:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Gel:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Drac:
                    case Tipus.Volador:
                    case Tipus.Herba:
                    case Tipus.Terra:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Psiquic:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Veri:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Normal:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Bicho:
                    case Tipus.Foc:
                    case Tipus.Ferro:
                    case Tipus.Volador:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Sinistre:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Fantasma:
                    case Tipus.Psiquic:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Veri:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Herba:
                    case Tipus.Psiquic:
                        multiplier *= 2;
                        break;
                }
                break;
            case Tipus.Volador:
                switch (atacado.m_Pok.m_Tipus)
                {
                    case Tipus.Bicho:
                    case Tipus.Herba:
                        multiplier *= 2;
                        break;
                }
                break;
        }

        return atacado.Recibir((int)(m_DMG * multiplier));
    }
}
