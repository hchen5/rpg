using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MainTrainer", menuName = "Pokemon/MainTrainer")]
public class MainTrainer : ScriptableObject
{
    public string m_Nom;
   //public Sprite m_Sprite;
    public PokemonCapturat[] m_Pokemons = new PokemonCapturat[6];
    public BackPack m_Inventari;
}
