using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokemonCapturat
{
    public string m_Nom;
    public Pokemon m_Pok;
    public int m_level;
    public int m_ExpRestant;
    public int m_VidaActual;
    public Atacs[] m_Atacs = new Atacs[4];
    public Sprite SpriteMini;
    public bool Vivo;

    
    public PokemonCapturat(string nom, Pokemon pok, int level, int expRestant, Atacs[] atacs, Sprite spriteMini, bool vivo)
    {
        m_Nom = nom;
        m_Pok = pok;
        m_level = level;
        m_ExpRestant = expRestant;
        m_Atacs = atacs;
        SpriteMini = spriteMini;
        Vivo = vivo;
    }

    public PokemonCapturat(string nom, Pokemon pok)
    {
        //m_Nom = nom;
        m_Nom = nom;
        m_Pok = pok;
        m_level = 1;
        m_ExpRestant = 100;
        m_Atacs = pok.m_Atacs;
        SpriteMini = pok.m_Sprite;
        m_VidaActual = pok.m_VidaMax;
        Vivo =true;
    }

    public void SubirNivel()
    {
        if (m_ExpRestant <= 0)
        {
            m_level++;
            int ExpSobrant = m_ExpRestant;
            m_ExpRestant = m_level * 1000 / 8;
            m_ExpRestant -= ExpSobrant;
        }
    }
    public void SeMuere()
    {
        Vivo = false;
    }
    public bool HealPokm(int Health)
    {
        if (m_VidaActual + Health <= m_Pok.m_VidaMax)
        {
            m_VidaActual += Health;
        }
        else
        {
            m_VidaActual = m_Pok.m_VidaMax;
        }
        if (m_VidaActual <= 0)

            return true;
        else
            return false;
    }
    public void Recibir(float dany)
    {
        m_VidaActual -= (int)dany;
    }
}
