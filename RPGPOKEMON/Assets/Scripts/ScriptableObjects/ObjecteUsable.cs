using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "ObjecteUsable", menuName = "Pokemon/ObjecteUsable")]
public class ObjecteUsable : Objecte
{
    public int m_VidaCura;

    public override void Use(PokemonCombat p)
    {
        p.m_VidaActual += m_VidaCura;
        if (p.m_VidaActual > p.m_Pok.m_VidaMax)
            p.m_VidaActual = p.m_Pok.m_VidaMax;
    }
}
