using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pokemon", menuName = "Pokemon/Pokemon")]
public class Pokemon : ScriptableObject
{
    public string m_Nom;
    public int m_NumPokedex;
    public string m_Desc;
    public Sprite m_Sprite;
    public Sprite m_SpriteF;
    public Tipus m_Tipus;
    public int m_VidaMax;
    public int m_Nivell;
    public Atacs[] m_Atacs = new Atacs[4];
    public ObjecteEquipable m_Objecte;
    public int m_ATK;
    public int m_SpATK;
    public int m_DEF;
    public int m_SpDEF;
    public int m_SPD;
    public Dictionary<int, Atacs> m_AtacsAprendibles = new Dictionary<int, Atacs>();

    
    
}
