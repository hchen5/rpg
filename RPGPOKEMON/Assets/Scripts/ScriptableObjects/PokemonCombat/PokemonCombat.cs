using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PokemonCombat", menuName = "Pokemon/PokemonCombat")]
public class PokemonCombat : ScriptableObject
{
    public string m_Nom;
    public Pokemon m_Pok;
    public int m_level;
    public int m_ExpRestant;
    public int m_VidaActual;
    public Atacs[] m_Atacs = new Atacs[4];
    public Sprite m_Sprite;
    public bool Vivo;

    /*
    public void SubirNivel()
    {
        if (m_ExpRestant <= 0)
        {
            m_level++;
            int ExpSobrant = m_ExpRestant;
            m_ExpRestant = m_level * 1000 / 8;
            m_ExpRestant -= ExpSobrant;
        }
    }
    public void SeMuere()
    {
        Vivo = false;
    }
    */
    public bool Recibir(int dany)
    {
        m_VidaActual -= dany;
        if (m_VidaActual<= 0)
        {
            m_VidaActual = 0;
            return true;
        }
        else
        {
            return false;
        }
        
    }
    public void LoadInfo(Pokemon p)
    {
        this.m_Pok = p;
        this.m_Nom = p.m_Nom;
        this.m_Atacs = p.m_Atacs;
        this.m_Sprite = p.m_Sprite;
        this.Vivo = true;
        this.m_VidaActual = p.m_VidaMax;
        this.m_level = p.m_Nivell;

    }

    public void LoadInfoAliat(PokemonCapturat p)
    {
        this.m_Pok = p.m_Pok;
        this.m_Nom = p.m_Nom;
        this.m_Atacs = p.m_Atacs;
        this.m_Sprite = p.m_Pok.m_Sprite;
        this.Vivo = true;
        this.m_VidaActual = p.m_VidaActual;
        this.m_level = p.m_Pok.m_Nivell;
        this.m_ExpRestant = p.m_ExpRestant;


    }

}
