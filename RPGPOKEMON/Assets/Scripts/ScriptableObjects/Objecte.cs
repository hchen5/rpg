using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Objecte : ScriptableObject
{
    public string m_Nom;

    public abstract void Use(PokemonCombat p);
}
