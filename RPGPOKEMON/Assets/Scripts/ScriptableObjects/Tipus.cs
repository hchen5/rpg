using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Tipus 
{
   Foc, Aigua, Herba, Electric, Terra, Gel, Volador, Ferro, Drac, Psiquic, Fantasma, Sinistre, Bicho, Veri, Normal  
}
