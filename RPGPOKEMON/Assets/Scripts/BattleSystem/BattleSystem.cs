using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor.Build.Content;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public enum STATE{ START,PLAYERTURN,ENEMYTURN,LOST,WIN}
public class BattleSystem :MonoBehaviour
{
    public STATE b_State;

    public Transform EnemyStation;
    public Transform PlayerStation;

    //cargar el pokemon salvaje
    [SerializeField]
    PokemonCombat m_WildPokemon;

    //cargar informacion del entrenador
    [SerializeField]
    MainTrainer m_Trainer;

    [SerializeField]
    PokemonCombat m_MyPokemon;

    //Classe Unit
    //Unit EnemyUnit;
    Unit PlayerUnit;

    public BattleHud enemyhud;
    public BattleHud playerhud;

    public ObjecteUsable[] pocions;

    

    public TextMeshProUGUI dialogo;

    //Botones 
    public GameObject Combatirbtn;
    public GameObject Huir;
    public GameObject Atack;
    public GameObject Atack2;
    public GameObject Atack3;
    public GameObject Atack4;
    public GameObject PotionS;
    public GameObject PotionL;


    public void UsePotionPetita()
    {
        if (m_MyPokemon.m_VidaActual < m_MyPokemon.m_Pok.m_VidaMax)
        {
            if (m_Trainer.m_Inventari.m_ObjectesUsables.ContainsKey(pocions[0]) &&
            m_Trainer.m_Inventari.m_ObjectesUsables[pocions[0]] > 0)
            {
                m_Trainer.m_Inventari.m_ObjectesUsables[pocions[0]]--;
                pocions[0].Use(m_MyPokemon);
                playerhud.setHP(m_MyPokemon.m_VidaActual);
                DesactivarPotionBtn();
                StartCoroutine(Advertencia("Pocion Peque�a Utilizada!"));
            }
        }
        else
            StartCoroutine(Advertencia("No puedes usar la pocion. Tu pokemon no esta herido!"));
    }
    
    public void UsePotionGran()
    {
        if (m_MyPokemon.m_VidaActual < m_MyPokemon.m_Pok.m_VidaMax)
        {
            if (m_Trainer.m_Inventari.m_ObjectesUsables.ContainsKey(pocions[1]) &&
            m_Trainer.m_Inventari.m_ObjectesUsables[pocions[1]] > 0)
            {
                m_Trainer.m_Inventari.m_ObjectesUsables[pocions[1]]--;
                pocions[1].Use(m_MyPokemon);
                playerhud.setHP(m_MyPokemon.m_VidaActual);
                DesactivarPotionBtn();
                StartCoroutine(Advertencia("Pocion Grande Utilizada!"));
            }
        }
        else
            StartCoroutine(Advertencia("No puedes usar la pocion. Tu pokemon no esta herido!"));
        
    }

    private IEnumerator Advertencia(String te)
    {
        string tmp = dialogo.text;
        dialogo.text = te;
        yield return new WaitForSeconds(1.4f);
        dialogo.text = tmp;
    }

    private void Start()
    {
        m_MyPokemon.LoadInfoAliat(m_Trainer.m_Pokemons[0]);
        Combatirbtn.SetActive(false);
        Huir.SetActive(false);
        if (m_MyPokemon.m_VidaActual > 0)
        {
            b_State = STATE.START;

            StartCoroutine(BattleSetUp());
        }
        else
        {
            b_State = STATE.START;

            
            if (m_Trainer.m_Inventari.m_ObjectesUsables.ContainsKey(pocions[0]) &&
          m_Trainer.m_Inventari.m_ObjectesUsables[pocions[0]] > 0)
            {
              m_Trainer.m_Inventari.m_ObjectesUsables[pocions[0]]--;
              pocions[0].Use(m_MyPokemon);
              playerhud.setHP(m_MyPokemon.m_VidaActual);
              dialogo.text = "Pocion Peque�a Utilizada!";
              StartCoroutine(BattleSetUp());
            }

            else if (m_Trainer.m_Inventari.m_ObjectesUsables.ContainsKey(pocions[1]) &&
               m_Trainer.m_Inventari.m_ObjectesUsables[pocions[1]] > 0)
            {
                m_Trainer.m_Inventari.m_ObjectesUsables[pocions[1]]--;
                pocions[1].Use(m_MyPokemon);
                playerhud.setHP(m_MyPokemon.m_VidaActual);
                dialogo.text = "Pocion Grande Utilizada!";
                StartCoroutine(BattleSetUp());
            }
            else
            {
                StartCoroutine(HuirNoPotions());
            }
            

        }
        
        
    }
    IEnumerator HuirNoPotions()
    {
        dialogo.text = "Tu pokemon no tiene vida y no tienes ninguna pocion. Huyes";
        EnemyStation.GetComponent<Image>().sprite = m_WildPokemon.m_Pok.m_SpriteF;
        PlayerStation.GetComponent<Image>().sprite = m_MyPokemon.m_Pok.m_Sprite;
        playerhud.setHUD(m_MyPokemon);
        enemyhud.setHUD(m_WildPokemon);
        yield return new WaitForSeconds(2f);
        HuirBtn();
    }
   
    IEnumerator BattleSetUp() 
    {
        dialogo.text = m_WildPokemon.m_Nom + " salvaje ha aparecido!";
        //Cargar los Sprites en las posiciones de la UI
        EnemyStation.GetComponent<Image>().sprite = m_WildPokemon.m_Pok.m_SpriteF;
        PlayerStation.GetComponent<Image>().sprite = m_MyPokemon.m_Pok.m_Sprite;

        //Pasar datos al Script de la hud
        playerhud.setHUD(m_MyPokemon);
        enemyhud.setHUD(m_WildPokemon);

        yield return new WaitForSeconds(2f);
        b_State = STATE.PLAYERTURN;
        PlayerTurn();
        
    }
    public void CombatirBtn()
    {
        //activar botones de ataque y desactivar boton huir y combatir
        Combatirbtn.SetActive(false);
        Huir.SetActive(false);
        ActivarAtackBtn();

    }
   
    public void HuirBtn() 
    {
        SaveInfo();
        //Funcion de huir y  volver a la escena principal
        SceneManager.LoadScene("Agustin");
        GameManager.Instance.LoadGame();
    }
    private void PlayerTurn()
    {
        ActivarPotionBtn();
        Combatirbtn.SetActive(true);
        Huir.SetActive(true);
        //dialogo
        dialogo.text = "Que quieres hacer ?";
     
    }
    
    public void AttackButton() 
    {
        Debug.Log("btn1");
        //dialogo.text = "Ataca ";
        if (b_State != STATE.PLAYERTURN)
            return;

        StartCoroutine(PlayerAtack());
    
    }

    public void Attack2Button() 
    {
        Debug.Log("btn2");

        if (b_State !=STATE.PLAYERTURN)
            return;
        StartCoroutine(PlayerAttack2());
    }
    public void Attack3Button() 
    {
        Debug.Log("btn3");

        if (b_State != STATE.PLAYERTURN)
            return;
        StartCoroutine(PlayerAttack3());
    }
    public void Attack4Button() 
    {
        Debug.Log("btn4");

        if (b_State != STATE.PLAYERTURN)
            return;
        StartCoroutine(PlayerAttack4());
    }
    IEnumerator PlayerAttack2()
    {
        dialogo.text = m_MyPokemon.m_Nom+" ha usado " + m_MyPokemon.m_Atacs[1].m_Nom + "!";
        yield return new WaitForSeconds(2f);
        StartCoroutine(FlashRedE());
        bool isDead = m_MyPokemon.m_Atacs[1].Ataquer(m_WildPokemon);
        enemyhud.setHP(m_WildPokemon.m_VidaActual);
        if (isDead)
        {
            b_State = STATE.WIN;
            StartCoroutine(EndBattle());
            DesactivarAtackBtn();
        }
        else 
        {
            b_State = STATE.ENEMYTURN;
            StartCoroutine(EnemyTurn());
            DesactivarAtackBtn();
        }

       
    }
    IEnumerator PlayerAttack3()
    {
        dialogo.text = m_MyPokemon.m_Nom + " ha usado " + m_MyPokemon.m_Atacs[2].m_Nom + "!";
        yield return new WaitForSeconds(2f);
        StartCoroutine(FlashRedE());
        bool isDead = m_MyPokemon.m_Atacs[2].Ataquer(m_WildPokemon);
        enemyhud.setHP(m_WildPokemon.m_VidaActual);
        if (isDead)
        {
            b_State = STATE.WIN;
            StartCoroutine(EndBattle());
            DesactivarAtackBtn();
        }
        else
        {
            b_State = STATE.ENEMYTURN;
            StartCoroutine(EnemyTurn());
            DesactivarAtackBtn();
        }
    }
    IEnumerator PlayerAttack4()
    {
        dialogo.text = m_MyPokemon.m_Nom + " ha usado " + m_MyPokemon.m_Atacs[3].m_Nom + "!";
        yield return new WaitForSeconds(2f);
        StartCoroutine(FlashRedE());
        bool isDead = m_MyPokemon.m_Atacs[3].Ataquer(m_WildPokemon);
        enemyhud.setHP(m_WildPokemon.m_VidaActual);
        if (isDead)
        {
            b_State = STATE.WIN;
            StartCoroutine(EndBattle());
            DesactivarAtackBtn();
        }
        else
        {
            b_State = STATE.ENEMYTURN;
            StartCoroutine(EnemyTurn());
            DesactivarAtackBtn();
        }
    }
  
    IEnumerator PlayerAtack()
    {
        dialogo.text = m_MyPokemon.m_Nom + " ha usado " + m_MyPokemon.m_Atacs[0].m_Nom+"!";
        yield return new WaitForSeconds(2f);
        StartCoroutine(FlashRedE());
        bool isDead = m_MyPokemon.m_Atacs[0].Ataquer(m_WildPokemon);
        enemyhud.setHP(m_WildPokemon.m_VidaActual);
        if (isDead)
        {
            b_State = STATE.WIN;
            StartCoroutine(EndBattle());
            DesactivarAtackBtn();
        }
        else
        {
            b_State = STATE.ENEMYTURN;
            StartCoroutine(EnemyTurn());
            DesactivarAtackBtn();
        }

    }

    void SaveInfo()
    {
        m_Trainer.m_Pokemons[0].m_level = m_MyPokemon.m_level;
        m_Trainer.m_Pokemons[0].m_ExpRestant = m_MyPokemon.m_ExpRestant;
        m_Trainer.m_Pokemons[0].m_VidaActual = m_MyPokemon.m_VidaActual;
        m_Trainer.m_Pokemons[0].Vivo = m_MyPokemon.Vivo;
    }
    void LevelUp()
    {
        if (m_MyPokemon.m_level < m_WildPokemon.m_level)
            m_MyPokemon.m_level += m_WildPokemon.m_level - m_MyPokemon.m_level;
        else
            m_MyPokemon.m_level += 1;
    }
    IEnumerator EndBattle()
    {
        LevelUp();
        SaveInfo();
        if (b_State == STATE.WIN)
        {
           
            //dialogo
            dialogo.text = "Has GANADO!";
            yield return new WaitForSeconds(3f);
            HuirBtn();
            DesactivarAtackBtn();

        }
        else
        {
            //dialogo
            dialogo.text = "Has PERDIDO!";
            yield return new WaitForSeconds(3f);
            HuirBtn();
            DesactivarAtackBtn();
        }
    }

    IEnumerator EnemyTurn()
    {
        dialogo.text = "Turno Enemigo";
        yield return new WaitForSeconds(2f);
        StartCoroutine(FlashRedA());
        int DMG = Random.Range(0, m_WildPokemon.m_Atacs.Length);
        bool isDead = m_MyPokemon.Recibir(m_WildPokemon.m_Atacs[DMG].m_DMG);
        Debug.Log("Ha usado :"+m_WildPokemon.m_Atacs[DMG].m_Nom);
        dialogo.text = m_WildPokemon.m_Nom+" enemigo ha usado " + m_WildPokemon.m_Atacs[DMG].m_Nom + "!";
        playerhud.setHP(m_MyPokemon.m_VidaActual);

        yield return new WaitForSeconds(2f);
        if (isDead)
        {
            b_State = STATE.LOST;
            StartCoroutine(EndBattle());
        }
        else
        {
            b_State = STATE.PLAYERTURN;
            PlayerTurn();
        }
    }
    //Activar Botones de Atacar
    public void ActivarAtackBtn() 
    {
        Atack.SetActive(true);
        Atack2.SetActive(true);
        Atack3.SetActive(true);
        Atack4.SetActive(true);
        Atack.GetComponentInChildren<TextMeshProUGUI>().text = m_MyPokemon.m_Pok.m_Atacs[0].m_Nom;
        Atack2.GetComponentInChildren<TextMeshProUGUI>().text = m_MyPokemon.m_Pok.m_Atacs[1].m_Nom;
        Atack3.GetComponentInChildren<TextMeshProUGUI>().text = m_MyPokemon.m_Pok.m_Atacs[2].m_Nom;
        Atack4.GetComponentInChildren<TextMeshProUGUI>().text = m_MyPokemon.m_Pok.m_Atacs[3].m_Nom;
    }
    //Desactivar Botones de Atacar
    public void DesactivarAtackBtn() 
    {
        Atack.SetActive(false);
        Atack2.SetActive(false);
        Atack3.SetActive(false);
        Atack4.SetActive(false);
        DesactivarPotionBtn();
    }

    public void ActivarPotionBtn()
    {
        if (m_Trainer.m_Inventari.m_ObjectesUsables.ContainsKey(pocions[0]) &&
           m_Trainer.m_Inventari.m_ObjectesUsables[pocions[0]] > 0)
        {
            PotionS.SetActive(true);
            PotionS.GetComponentInChildren<TextMeshProUGUI>().text = ""+m_Trainer.m_Inventari.m_ObjectesUsables[pocions[0]];
        }

        if (m_Trainer.m_Inventari.m_ObjectesUsables.ContainsKey(pocions[1]) &&
           m_Trainer.m_Inventari.m_ObjectesUsables[pocions[1]] > 0)
        {
            PotionL.SetActive(true); 
            PotionL.GetComponentInChildren<TextMeshProUGUI>().text = "" + m_Trainer.m_Inventari.m_ObjectesUsables[pocions[1]];

        }
    }

    public void DesactivarPotionBtn()
    {
        PotionL.SetActive(false);
        PotionS.SetActive(false);
    }




    public IEnumerator FlashRedA()
    {
        Color tmp = Color.white;
        tmp.a = 0f;
        PlayerStation.GetComponent<Image>().color = tmp;
        yield return new WaitForSeconds(0.1f);
        PlayerStation.GetComponent<Image>().color = Color.white;
        yield return new WaitForSeconds(0.1f);
        PlayerStation.GetComponent<Image>().color = tmp;
        yield return new WaitForSeconds(0.1f);
        PlayerStation.GetComponent<Image>().color = Color.white;
    }

    public IEnumerator FlashRedE()
    {
        Color tmp = Color.white;
        tmp.a = 0f;
        EnemyStation.GetComponent<Image>().color = tmp;
        yield return new WaitForSeconds(0.1f);
        EnemyStation.GetComponent<Image>().color = Color.white;
        yield return new WaitForSeconds(0.1f);
        EnemyStation.GetComponent<Image>().color = tmp;
        yield return new WaitForSeconds(0.1f);
        EnemyStation.GetComponent<Image>().color = Color.white;
    }
}
