using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class BattleHud : MonoBehaviour
{
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI LevelText;
    public Slider hpSlider;

    public void setHUD(PokemonCombat pok) 
    {
        NameText.text = pok.m_Nom;
        LevelText.text = ""+pok.m_Pok.m_Nivell;
        hpSlider.maxValue = pok.m_Pok.m_VidaMax;
        hpSlider.value = pok.m_VidaActual;
            
    }
    private void Awake()
    {
        hpSlider.enabled = false;
    }
    /*
    public void setHUDMyPKMN(PokemonCombat pok)
    {
        NameText.text = pok.m_Nom;
        LevelText.text = "Lvl"+pok.m_level;
        hpSlider.maxValue = pok.m_Pok.m_VidaMax;
        hpSlider.value = pok.m_VidaActual;

    }
    */
    public void setHP(int hp) 
    {
        hpSlider.value = hp;
    }
}
