using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public struct SavePlayerPosition 
{
    //public string level;
    [Serializable]
    public struct SaveObjectPositionRotation
    {
        public Vector3 position;
        public Vector3 position2;
        public PokemonCapturat[] pokemons;
        public string pokemonname;
        public string pokemonnamepers;

    }
    public SaveObjectPositionRotation player;

}
