using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : MonoBehaviour
{

    Rigidbody2D m_RigidBody;
    Animator m_animator;

    private int m_dirN = 0;
    public readonly string[] IdleDirections = { "IdleW", "IdleWD", "IdleD", "IdleSD", "IdleS", "IdleSA", "IdleA", "IdleWA" };
    public readonly string[] RunDirections = { "RunW", "RunWD", "RunD", "RunSD", "RunS", "RunSA", "RunA", "RunWA" };
    //   0         1       2       3       4       5       6       7
    [SerializeField]
    private float CharacterSpeedWalk = 1.0f;
    [SerializeField]
    private float CharacterSpeedRun = 2.0f;
    private float CharacterSpeedActual = 1.0f;

    private Vector2 InputDir;
    private float VertInput;
    private float HorInput;
    private Vector2 WhereAmI;
    private Vector2 WhereTo;

    public bool canMove = true;



    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (canMove)
        {
            VertInput = Input.GetAxis("Vertical");
            HorInput = Input.GetAxis("Horizontal");
            if (Input.GetKey(KeyCode.Space))
                CharacterSpeedActual = CharacterSpeedRun;
            else
                CharacterSpeedActual = CharacterSpeedWalk;
            if (InputDir != new Vector2(HorInput, VertInput).normalized)
            {
                InputDir = new Vector2(HorInput, VertInput).normalized;
                CheckDireccio(InputDir);
            }
            WhereAmI = m_RigidBody.position;
            WhereTo = WhereAmI + (InputDir * CharacterSpeedActual) * Time.fixedDeltaTime;
            m_RigidBody.MovePosition(WhereTo);
        }
        else
        {
            m_animator.Play(IdleDirections[m_dirN]);
        }

    }


    void CheckDireccio(Vector2 direccion)
    {
        if (direccion.x < 0)            //va a la izquierda
        {
            if (direccion.y > 0)        //va arriba = WA
            {
                m_dirN = 7;
            }
            else if (direccion.y < 0)   //va abajo = SA
            {
                m_dirN = 5;
            }
            else                        //no va vertical = A
            {
                m_dirN = 6;
            }

        }
        else if (direccion.x > 0)          //va a la derecha
        {
            if (direccion.y > 0)        //va arriba = WD
            {
                m_dirN = 1;
            }
            else if (direccion.y < 0)   //va abajo = SD
            {
                m_dirN = 3;
            }
            else                        //no va vertical = D
            {
                m_dirN = 2;
            }
        }
        else                            //no va a lados
        {
            if (direccion.y > 0)        //va arriba = W
            {
                m_dirN = 0;
            }
            else if (direccion.y < 0)   // va abajo = S
            {
                m_dirN = 4;
            }
            else                        //no va vertical = NADA
            {
                m_animator.Play(IdleDirections[m_dirN]);
                return;
                //tendria q quedarse quieto mirando a donde iba
            }
        }
        m_animator.Play(RunDirections[m_dirN]);
    }


}
